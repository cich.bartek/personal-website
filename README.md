# Personal Website
Link to my website: [https://bartek33642.github.io/my-website/](https://bartek33642.github.io/my-website/)
## My personal website
Create in HTML, CSS & Javascript

## Pics
![](https://gitlab.com/cich.bartek/personal-website/-/raw/develop/git_img/1.png)
![](https://gitlab.com/cich.bartek/personal-website/-/raw/develop/git_img/2.png)
![](https://gitlab.com/cich.bartek/personal-website/-/raw/develop/git_img/3.png)
![](https://gitlab.com/cich.bartek/personal-website/-/raw/develop/git_img/4.png)
